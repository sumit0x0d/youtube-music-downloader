"""
YouTube Music Downloader
"""

import hashlib
import itertools
import json
import logging
import re
import sys
import time
import subprocess

from http.cookiejar import MozillaCookieJar
from pathlib import Path

from requests import get, post, Session, utils
from requests.models import HTTPError

YTMUSIC_URL = "https://music.youtube.com"
INNERTUBE_API_KEY = "AIzaSyC9XL3ZjWddXya6X74dJoCTL-WEYFDNX30"

INNERTUBE_CLIENT_NAME = "WEB_REMIX"
INNERTUBE_CLIENT_VERSION = "0.1"

USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)Chrome/91.0.4472.101 Safari/537.36"


class Source:
	"""
	signatureTimestamp
	ytPlayerFunctionTransform
	ytPlayerObjectTransform
	"""

	def __init__(self, videoId):
		log = logging.getLogger("Source")
		url = f"{YTMUSIC_URL}/watch?v={videoId}"
		headers = {
			"origin": YTMUSIC_URL,
			"user-agent": USER_AGENT,
		}
		log.info(" Downloading Response...")
		response = self.__getResponse(log, url, headers)
		log.info(" Response Downloaded")
		jsUrl = self.__getJsUrl(response)
		ytPlayer = self.__getYtPlayer(jsUrl)
		self.signatureTimestamp = self.__getSignatureTimestamp(ytPlayer)
		ytPlayerFunctionName = self.__getYtPlayerFunctionName(ytPlayer)
		ytPlayerFunction = self.__getYtPlayerFunction(ytPlayer, ytPlayerFunctionName)
		ytPlayerObjectName = self.__getYtPlayerObjectName(ytPlayerFunction)
		ytPlayerObject = self.__getYtPlayerObject(ytPlayer, ytPlayerObjectName)
		self.ytPlayerFunctionTransform = self.__getYtPlayerFunctionTransform(ytPlayerFunction)
		self.ytPlayerObjectTransform = self.__getYtPlayerObjectTransform(ytPlayerObject)

	@staticmethod
	def __getResponse(log, url, headers):
		try:
			response = get(
				url=url,
				headers=headers,
				timeout=4
			).content.decode("UTF-8")
		except HTTPError:
			log.error(" Response Downloading Failed")
			sys.exit()
		return response

	@staticmethod
	def __getJsUrl(response):
		pattern = r"(/s/player/[\w\d]+/[\w\d_/.]+/base\.js)"
		jsUrl = re.compile(pattern).search(response)
		jsUrl = YTMUSIC_URL + jsUrl.group(0)
		return jsUrl

	@staticmethod
	def __getYtPlayer(jsUrl):
		try:
			ytPlayer = get(url=jsUrl, timeout=4)
		except HTTPError:
			sys.exit()
		ytPlayer = ytPlayer.content.decode("UTF-8")
		return ytPlayer

	@staticmethod
	def __getSignatureTimestamp(ytPlayer):
		pattern = r"signatureTimestamp[:=](\d+)"
		signatureTimestamp = re.compile(pattern).search(ytPlayer).group(1)
		return signatureTimestamp

	@staticmethod
	def __getYtPlayerFunctionName(ytPlayer):
		pattern = r'(?P<sig>[a-zA-Z0-9$]+)\s*=\s*function\(\s*a\s*\)\s*{\s*a\s*=\s*a\.split\(\s*""\s*\)'
		ytPlayerFunctionName = re.compile(pattern).search(ytPlayer).group(1)
		return ytPlayerFunctionName

	@staticmethod
	def __getYtPlayerFunction(ytPlayer, ytPlayerFunctionName):
		pattern = r"%s=function\(\w\){[a-z=\.\(\"\)]*;(.*);(?:.+)};" % ytPlayerFunctionName
		ytPlayerFunction = re.compile(pattern).search(ytPlayer).group(0)
		return ytPlayerFunction

	@staticmethod
	def __getYtPlayerFunctionTransform(ytPlayerFunction):
		pattern = r"{\w=\w\.split\(\"\"\);(.*);return\s\w\.join\(\"\"\)};"
		ytPlayerFunctionTransform = re.compile(pattern).search(ytPlayerFunction).group(1).split(";")
		return ytPlayerFunctionTransform

	@staticmethod
	def __getYtPlayerObjectName(ytPlayerFunction):
		pattern = r"\w+\.\w+\(\w\,\w\)"
		ytPlayerObjectName = re.compile(pattern).search(ytPlayerFunction).group().split(".")[0]
		return ytPlayerObjectName

	@staticmethod
	def __getYtPlayerObject(ytPlayer, ytPlayerObjectName):
		pattern = r"var\s*%s={([\S\s]*?)};" % ytPlayerObjectName
		ytPlayerObject = re.compile(pattern).search(ytPlayer).group(0)
		return ytPlayerObject

	@staticmethod
	def __getYtPlayerObjectTransform(ytPlayerObject):
		pattern = r"{([\S\s]*?)};"
		ytPlayerObjectTransform = re.compile(pattern).search(ytPlayerObject).group(1).replace("\n", " ")
		ytPlayerObjectTransform = ytPlayerObjectTransform..split(", ")
		return ytPlayerObjectTransform


class Player:
	"""
	downloadUrl
	thumbnailUrl
	"""

	def __init__(self, videoId, source, itag):
		log = logging.getLogger("Player")
		session = Session()
		if itag == 141:
			cookies_txt = Path("./cookies.txt")
			if cookies_txt.is_file():
				with open("cookies.txt", "r") as file:
					data = file.read().replace("#HttpOnly_", "")
				with open("cookies.txt", "w") as file:
					file.write(data)
				session.cookies = MozillaCookieJar("cookies.txt")
				session.cookies.load(ignore_discard=True, ignore_expires=True)
			else:
				logging.error("Provide cookies.txt")
				sys.exit()
		url = f"{YTMUSIC_URL}/youtubei/v1/player?key={INNERTUBE_API_KEY}"
		data = {
			"videoId": videoId,
			"context": {
				"client": {
					"clientName": INNERTUBE_CLIENT_NAME,
					"clientVersion": INNERTUBE_CLIENT_VERSION,
				}
			},
			"playbackContext": {
				"contentPlaybackContext": {
					"signatureTimestamp": source.signatureTimestamp,
				}
			}
		}
		authorization = self.__getAuthorization(session, itag)
		headers = {
			"origin": YTMUSIC_URL,
			"user-agent": USER_AGENT,
			"authorization": authorization
		}
		log.info(" Downloading Response...")
		response = self.__getResponse(log, session, url, data, headers)
		log.info(" Response Downloaded")
		if itag == 141:
			session.cookies.save(ignore_discard=True, ignore_expires=True)
		session.close()
		log.info(" Generating Download & Thumbnail URL...")
		signatureCipher = self.__getSignatureCipher(response, itag)
		signatureCipherS = signatureCipher["s"]
		signatureCipherUrl = signatureCipher["url"]
		transformMap = self.__transformMap(source.ytPlayerObjectTransform)
		sig = self.__getSig(source.ytPlayerFunctionTransform, signatureCipherS, transformMap)
		self.downloadUrl = self.__getDownloadUrl(signatureCipherUrl, sig)
		log.info(" Download URL Generated")
		self.thumbnailUrl = self.__getThumbnailUrl(response)
		log.info(" Thumbnail URL Generated")

	@staticmethod
	def __getAuthorization(session, itag):
		if itag == 141:
			currentTime = str(int(time.time()))
			sapisid = session.cookies.__dict__["_cookies"][".youtube.com"]["/"]["SAPISID"].value
			sapisidhash = hashlib.sha1(" ".join([currentTime, sapisid, YTMUSIC_URL]).encode()).hexdigest()
			authorization = f"SAPISIDHASH {currentTime}_{sapisidhash}"
			return authorization
		return None

	@staticmethod
	def __getResponse(log, session, url, data, headers):
		try:
			response = session.post(
				url=url,
				data=json.dumps(data),
				headers=headers,
				timeout=4
			).json()
		except HTTPError:
			log.error(" Response Downloading Failed")
			sys.exit()
		return response

	@staticmethod
	def __getSignatureCipher(response, itag):
		signatureCipherSPattern = r"s=(.*)&sp=sig&"
		signatureCipherUrlPattern = r"&sp=sig&url=(.*)"
		adaptiveFormats = response["streamingData"]["adaptiveFormats"]
		for adaptiveFormat in adaptiveFormats:
			if adaptiveFormat["itag"] == itag:
				signatureCipher = adaptiveFormat["signatureCipher"]
				signatureCipherS = re.compile(signatureCipherSPattern).search(signatureCipher)
				signatureCipherS = utils.unquote(signatureCipherS.group(1))
				signatureCipherUrl = re.compile(signatureCipherUrlPattern).search(signatureCipher)
				signatureCipherUrl = utils.unquote(signatureCipherUrl.group(1))
		if not signatureCipherS or not signatureCipherUrl:
			sys.exit()
		signatureCipher = {"s": signatureCipherS, "url": signatureCipherUrl}
		return signatureCipher

	@staticmethod
	def __reverse(_a, _):
		return _a[::-1]

	@staticmethod
	def __splice(_a, _b):
		return _a[_b:]

	@staticmethod
	def __swap(_a, _b):
		_r = _b % len(_a)
		return list(itertools.chain([_a[_r]], _a[1:_r], [_a[0]], _a[_r + 1:]))

	def __mapFunction(self, ytPlayerObject):
		patternFunctionList = (
			(r"{\w\.reverse\(\)}", self.__reverse),
			(r"{\w\.splice\(0,\w\)}", self.__splice),
			(r"{var\s\w=\w\[0\];\w\[0\]=\w\[\w%\w.length\];\w\[\w\]=\w}", self.__swap),
			(r"{var\s\w=\w\[0\];\w\[0\]=\w\[\w%\w.length\];\w\[\w\%\w.length\]=\w}", self.__swap)
		)
		for pattern, function in patternFunctionList:
			if re.compile(pattern).search(ytPlayerObject):
				return function
		return None

	def __transformMap(self, ytPlayerObjectTransform):
		transformMap = {}
		for i in ytPlayerObjectTransform:
			name, yt_player_object = i.split(":", 1)
			transformMap[name] = self.__mapFunction(yt_player_object)
		return transformMap

	@staticmethod
	def __parseFunction(function):
		patterns = [
			r"\w+\.(\w+)\(\w,(\d+)\)",
			r"\w+\[(\"\w+\")\]\(\w,(\d+)\)"
		]
		for pattern in patterns:
			regex = re.compile(pattern).search(function)
			if regex:
				name, arg = regex.groups()
				return name, int(arg)
		return None

	def __getSig(self, ytPlayerFunctionTransform, signatureCipherS, transformMap):
		sig = list(signatureCipherS)
		for function in ytPlayerFunctionTransform:
			name, arg = self.__parseFunction(function)
			sig = transformMap[name](sig, arg)
		sig = "".join(sig)
		return sig

	@staticmethod
	def __getDownloadUrl(signatureCipherUrl, sig):
		downloadUrl = f"{signatureCipherUrl}&sig={sig}"
		return downloadUrl

	@staticmethod
	def __getThumbnailUrl(response):
		thumbnailUrl = response["videoDetails"]["thumbnail"]["thumbnails"][0]["url"].split("=")[0]
		return thumbnailUrl


class Next:
	"""
	title
	artistName
	albumName
	albumId
	year
	"""

	def __init__(self, videoId):
		log = logging.getLogger("Next")
		url = f"{YTMUSIC_URL}/youtubei/v1/next?key={INNERTUBE_API_KEY}"
		data = {
			"videoId": videoId,
			"context": {
				"client": {
					"clientName": INNERTUBE_CLIENT_NAME,
					"clientVersion": INNERTUBE_CLIENT_VERSION,
				}
			}
		}
		headers = {
			"origin": YTMUSIC_URL,
		}
		log.info(" Downloading Response...")
		response = self.__getResponse(log, url, data, headers)
		log.info(" Response Downloaded")
		playlistPanelVideoRenderer = response["contents"]["singleColumnMusicWatchNextResultsRenderer"]
		playlistPanelVideoRenderer = playlistPanelVideoRenderer["tabbedRenderer"]
		playlistPanelVideoRenderer = playlistPanelVideoRenderer["watchNextTabbedResultsRenderer"]["tabs"][0]
		playlistPanelVideoRenderer = playlistPanelVideoRenderer["tabRenderer"]["content"]["musicQueueRenderer"]
		playlistPanelVideoRenderer = playlistPanelVideoRenderer["content"]["playlistPanelRenderer"]["contents"]
		playlistPanelVideoRenderer = playlistPanelVideoRenderer[0]["playlistPanelVideoRenderer"]
		self.title = self.__getTitle(playlistPanelVideoRenderer)
		self.artist = self.__getArtist(playlistPanelVideoRenderer)
		album = self.__getAlbum(playlistPanelVideoRenderer)
		self.albumName = album["name"]
		self.albumId = album["id"]
		self.year = self.__getYear(playlistPanelVideoRenderer)

	@staticmethod
	def __getResponse(log, url, data, headers):
		try:
			response = post(
				url=url,
				data=json.dumps(data),
				headers=headers,
				timeout=4
			).json()
		except HTTPError:
			log.error(" Response Downloading Failed")
			sys.exit()
		return response

	@staticmethod
	def __getTitle(playlistPanelVideoRenderer):
		title = playlistPanelVideoRenderer["title"]["runs"][0]["text"]
		return title

	@staticmethod
	def __getArtist(playlistPanelVideoRenderer):
		runs = playlistPanelVideoRenderer["longBylineText"]["runs"]
		artist = []
		j = 0
		for i in range(0, len(runs) - 4, 2):
			artist.append(runs[i]["text"])
			j = j + 1
		return artist

	@staticmethod
	def __getAlbum(playlistPanelVideoRenderer):
		name = playlistPanelVideoRenderer["longBylineText"]["runs"][-3]["text"]
		_id = playlistPanelVideoRenderer["longBylineText"]["runs"][-3]["navigationEndpoint"]["browseEndpoint"]
		_id = _id["browseId"]
		album = {"name": name, "id": _id}
		return album

	@staticmethod
	def __getYear(playlistPanelVideoRenderer):
		year = playlistPanelVideoRenderer["longBylineText"]["runs"][-1]["text"]
		return year


class Browse:
	"""
	albumTrackIndex
	albumTrackCount
	"""

	def __init__(self, _next):
		url = f"{YTMUSIC_URL}/youtubei/v1/browse?key={INNERTUBE_API_KEY}"
		headers = {
			"origin": YTMUSIC_URL,
		}
		data = {
			"browseId": "",
			"context": {
				"client": {
					"clientName": INNERTUBE_CLIENT_NAME,
					"clientVersion": INNERTUBE_CLIENT_VERSION,
				}
			}
		}
		albumTrack = self.__getAlbumTrack(_next.albumId, _next.title, url, data, headers)
		self.albumTrackIndex = albumTrack["index"]
		self.albumTrackCount = albumTrack["count"]

	@staticmethod
	def __getResponse(log, _id, url, data, headers):
		data.update({"browseId": _id})
		try:
			response = post(
				url=url,
				data=json.dumps(data),
				headers=headers,
				timeout=4
			).json()
		except HTTPError:
			log.error(" Response Downloading Failed")
			sys.exit()
		return response

	def __getAlbumTrack(self, albumId, title, url, data, headers):
		log = logging.getLogger("Browse Album Track")
		log.info(" Downloading Response...")
		response = self.__getResponse(log, albumId, url, data, headers)
		log.info(" Response Downloaded")
		mutations = response["frameworkUpdates"]["entityBatchUpdate"]["mutations"]
		for i in mutations:
			if "musicTrack" in i["payload"]:
				if i["payload"]["musicTrack"]["title"] == title:
					index = i["payload"]["musicTrack"]["albumTrackIndex"]
		count = mutations[-1]["payload"]["musicAlbumRelease"]["trackCount"]
		albumTrack = {
			"index": index,
			"count": count,
		}
		return albumTrack


def download(player, _next, browse=None):
	"""Download"""
	title = f"{_next.title}".replace("/", "-")
	if len(_next.artist) == 1:
		artist = _next.artist[0]
	else:
		artist = ", ".join(_next.artist[:-1]) + " & " + _next.artist[-1]
	logging.info(" Downloading Track...")
	response = get(player.downloadUrl)
	open(f"{title}", "wb").write(response.content)
	# Path(f"{_next.albumName}").mkdir(parents=True, exist_ok=True)
	# subprocess.run([
	# 	"ffmpeg", "-y", "-i", f"{player.downloadUrl}",
	# 	"-i", f"{player.thumbnailUrl}",
	# 	"-map", "0", "-map", "1",
	# 	"-disposition:1", "attached_pic",
	# 	"-metadata", f"title={_next.title}",
	# 	"-metadata", f"artist={artist}",
	# 	"-metadata", f"album={_next.albumName}",
	# 	"-metadata", f"date={_next.year}",
	# 	# "-metadata", f"track={browse.albumTrackindex}/{browse.albumTrackcount}",
	# 	"-codec", "copy", "-f", "mp4",
	# 	f"{_next.albumName}/{title}.m4a"
	# ], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, check=True)
	logging.info(" Track Downloaded")


def main(videoId, itag):
	"""Main"""
	if itag not in (140, 141):
		sys.exit()
	download(Player(videoId, Source(videoId), itag), Next(videoId))


if __name__ == "__main__":
	logging.basicConfig(level=logging.INFO)
	main(sys.argv[-2], int(sys.argv[-1]))
